package FillingJar;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Please enter the number of jars");
		int n = in.nextInt();
		List<int[]> m = new ArrayList<int[]>();
		int[] arr1 = {1,2,10};
		int[] arr2 = {3,5,10};
		m.add(arr1);
		m.add(arr2);
		System.out.println(calculateAverage(n, m));	

	}
	public static float calculateAverage(int n,List<int[]> m) {
		int[] numOfCandyInJar = new int[n];
		for(int i = 0;i<m.size();i++) {
			int[] arr = m.get(i);
			int a = arr[0];
			int b = arr[1];
			int k = arr[2];
			for(int j = a;j<=b;j++) {
				numOfCandyInJar[j-1] += k;
			}	
		}
		int sum = 0;
		for(int i = 0;i<n;i++) {
			sum += numOfCandyInJar[i];
		}
		
		float retAverage = (float) sum / n;
		
		return retAverage;	
		
	}

}
